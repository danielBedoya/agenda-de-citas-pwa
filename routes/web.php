<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (App::environment('production')) {
    URL::forceScheme('https');
}

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'adminController@index')->name('admin');
Route::get('/register/{quien}', 'adminController@register')->name('register1');
Route::post('/saveuser/{quien}', 'adminController@saveuser')->name('saveuser');
Route::get('/show/{quien}', 'adminController@show')->name('show');
Route::get('/showedit/{id}', 'adminController@showedit')->name('showedit');
Route::post('/edit/{id}', 'adminController@edit')->name('edit');
Route::get('/delete/{id}', 'adminController@delete')->name('delete');
Route::get('/registercita', 'adminController@registercita')->name('regcita');
Route::post('/savecita', 'adminController@savecita')->name('savecita');
Route::get('/showcitas', 'adminController@showcitas')->name('showcitas');
Route::get('/showcita/{id}', 'adminController@showcita')->name('showcita');
Route::post('/editcita/{id}', 'adminController@editcita')->name('editcita');
Route::get('/deletecita/{id}', 'adminController@deletecita')->name('deletecita');

Route::get('/offline', function () {    
    return view('modules/laravelpwa/offline');
});
