<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Citas @yield('title')</title>

    <!-- CSS Bootstrap v4.4 -->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">

    <!-- CSS Font Awesome v5.12 -->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/font-awesome/css/all.min.css') }}">

    <!-- CSS Data Tables v1.10 -->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/datatables/css/jquery.dataTables.min.css') }}">

    <!-- Page's Stylesheets -->
    @yield('stylesheets')

    @laravelPWA

</head>

<body>

    <!-- Start Main Content -->
    @yield('content')
    <!-- End Main Content -->

    <!-- JQuery v3.4 -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>

    <!-- JS Bootstrap v4.4 -->
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- JS Data Tables v1.10 -->
    <script src="{{ asset('vendor/datatables/js/jquery.dataTables.min.js') }}"></script>

    <!-- Page's Scripts -->
    @yield('javascript')

</body>

</html>