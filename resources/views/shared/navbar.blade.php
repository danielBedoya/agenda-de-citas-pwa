<nav class="navbar navbar-expand-lg bg-light border-bottom shadow px-3 fixed-top">
    <div class="container">
        <div class="navbar-header">
        
            <div class="col-auto">
                <a href="{{route('admin')}}"><h3>Inicio</h3></a>
            </div>
        
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right ml-auto">
                <li class="dropdown mt-2">
                    <a href="#" class="dropdown-toggle text-dark" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                        {{ Auth::user()->name }} <span class="caret">Opciones</span>
                    </a>

                    <ul class="dropdown-menu">
                        <li>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>