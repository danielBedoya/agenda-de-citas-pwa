@extends('layouts.main')

@section('content')
@include('shared.navbar')
    <section class="section-view ">
        <div class="container" style="margin-top: 10%">
            <div class="row ">
                <div class="mx-auto col-auto text-center">
                    <table id="usersTable">
                        <thead class="bg-danger">
                            <tr>
                                <th scope="col">Primer nombre</th>
                                <th scope="col">Segundo nombre</th>
                                <th scope="col">Primer apellido</th>
                                <th scope="col">Segundo apellido</th>
                                <th scope="col">Correo</th>
                                <th scope="col">Número documento</th>
                                @if($quien == 'medico')
                                    <th scope="col">Número tarjeta profesional</th>
                                    <th scope="col">Especialidad</th>
                                @endif
                                <th scope="col">Teléfono</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{$user->firstname}}</td>
                                    <td>{{$user->secondname}}</td>
                                    <td>{{$user->surname}}</td>
                                    <td>{{$user->secondsurname}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->numberdoc}}</td>
                                    @if($quien == 'medico')
                                        <td>{{$user->numbertarjet}}</td>
                                        <td>{{$user->especialidad}}</td>
                                    @endif
                                    <td>{{$user->telefono}}</td>
                                    <td>
                                        <a href=" {{ route('showedit',$user->id) }} "> <div class="btn btn-primary"> Editar </div></a>
                                        <a href=" {{ route('delete',$user->id) }} "> <div class="btn btn-primary">Eliminar </div></a>
                                    </td>
                                </tr>  
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('javascript')
    <script type="text/javascript">
        $('#usersTable').DataTable();
    </script>
@endsection