@extends('layouts.main')

@section('content')
@include('shared.navbar')
    <section class="section-view ">
        <div class="container" style="margin-top: 5%">
            <h3 class="text-center" id="rg-paciente">Registrar Paciente</h3>
            <h3 class="text-center" id="rg-medico">Registrar Medico</h3>
            <form class="mx-auto col-10 mt-4" method="POST" action="{{route('saveuser',$quien)}}">
                {{csrf_field()}}
                <div class="row">
                    <div class="mx-auto form-group col-4">
                        <label for="firstname" class="text-dark">Primer nombre</label>
                        <input type="text" class="form-control" id="firstname" name="firstname" required>
                        <label for="secondname" class="text-dark mt-2">Segundo nombre</label>
                        <input type="text" class="form-control" id="secondname" name="secondname" >
                        <label for="surname" class="text-dark mt-2">Primer apellido</label>
                        <input type="text" class="form-control" id="surname" name="surname" required>
                        <label for="secondsurname" class="text-dark mt-2">Segundo apellido</label>
                        <input type="text" class="form-control" id="secondsurname" name="secondsurname" >
                        <label for="email" class="text-dark mt-2">Correo electrónico</label>
                        <input type="email" class="form-control" id="email" name="email" required>
                    </div>
                    <div class="mx-auto form-group col-4">
                        <label for="numberdoc" class="text-dark">Número documento</label>
                        <input type="number" class="form-control" id="numberdoc" name="numberdoc" required>
                        @if($quien == "medico")
                            <div id="rg-medico1">
                                <label for="numbertarjet" class="text-dark mt-2">Número tarjeta profesional</label>
                                <input type="number" class="form-control" id="numbertarjet" name="numbertarjet" required>
                                <label for="especialidad" class="text-dark mt-2">Especialidad</label>
                                <input type="text" class="form-control" id="especialidad" name="especialidad" required>
                            </div>
                        @endif
                        <label for="password" class="text-dark mt-2">Contraseña</label>
                        <input type="password" class="form-control" id="password" name="passwor" required>
                        <label for="telefono" class="text-dark mt-2">Teléfono</label>
                        <input type="number" class="form-control" id="telefono" name="telefono" >
                    </div>
                </div>
                <div class="row">
                    <div class="mx-auto col-auto">
                        <input type="submit" value="Registrar" class="btn btn-lg btn-outline-primary">
                    </div>
                </div>
            </form>
        </div>
    </section>

@endsection
@section('javascript')
    <script type="text/javascript">
        if("{{$quien}}" == 'paciente'){
            document.getElementById('rg-medico').style.display = 'none';
            document.getElementById('rg-medico1').style.display = 'none';
        }
        else{
            document.getElementById('rg-paciente').style.display = 'none';
        }
    </script>
@endsection