@extends('layouts.main')

@section('content')
    <section class="section-view ">
        @if(isset($updated) and $updated == 'true')
            <div class="alert alert-success" role="alert">
                Se ha guardado satisfactoriamente.
            </div>
        @endif
        @if(isset($deleted) and $deleted == 'true')
            <div class="alert alert-success" role="alert">
                Se ha eliminado satisfactoriamente.
            </div>
        @endif
        <div class="container" style="margin-top: 20%">
            <div class="row">
                <div class="mx-auto col-auto">
                    <a href=" {{route('register1','paciente')}} "><div class="btn btn-outline-primary"> Añadir paciente</div></a>
                    <a href=" {{route('register1','medico')}} "><div class="btn btn-outline-primary"> Añadir medico</div></a>
                    <a href=" {{route('regcita')}} "><div class="btn btn-outline-primary"> Añadir cita</div></a>
                </div>
            </div>
            
            <div class="row mt-1">
                <div class="mx-auto mt-2 col-auto">
                    <a href=" {{route('show','paciente')}} "><div class="btn btn-outline-primary"> Modificar paciente</div></a>
                    <a href=" {{route('show','medico')}} "><div class="btn btn-outline-primary"> Modificar medico</div></a>
                    <a href=" {{route('showcitas')}} "><div class="btn btn-outline-primary"> Modificar cita</div></a>
                </div>
            </div>
            
        </div>
    </section>

@endsection