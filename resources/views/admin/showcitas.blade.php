@extends('layouts.main')

@section('content')
@include('shared.navbar')
    <section class="section-view ">
        <div class="container" style="margin-top: 10%">
            <div class="row ">
                <div class="mx-auto col-auto text-center">
                    <table id="usersTable">
                        <thead class="bg-danger">
                            <tr>
                                <th scope="col">Medico</th>
                                <th scope="col">Paciente</th>
                                <th scope="col">Fecha</th>
                                <th scope="col">Hora</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($citas as $cita)
                                <tr>
                                    <td>{{$cita->medico_info->firstname}} {{$cita->medico_info->surname}}</td>
                                    <td>{{$cita->paciente_info->firstname}} {{$cita->paciente_info->surname}}</td>
                                    <td>{{$cita->fecha}}</td>
                                    <td>{{$cita->hora}}</td>
                                    <td>
                                        <a href=" {{ route('showcita',$cita->id) }} "> <div class="btn btn-primary"> Editar </div></a>
                                        <a href=" {{ route('deletecita',$cita->id) }} "> <div class="btn btn-primary">Eliminar </div></a>
                                    </td>
                                </tr>  
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('javascript')
    <script type="text/javascript">
        $('#usersTable').DataTable();
    </script>
@endsection