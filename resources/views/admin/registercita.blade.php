@extends('layouts.main')

@section('content')
@include('shared.navbar')
    <section class="section-view ">
        <div class="container" style="margin-top: 5%">
            <h3 class="text-center" id="rg-paciente">Registrar Cita</h3>
            <form class="mx-auto col-6 mt-4" method="POST"  @if( isset($cita) ) action=" {{ route('editcita',$cita->id) }} " @else action="{{route('savecita')}}" @endif>
                {{csrf_field()}}
                <div class="row">
                    <label for="selectPaciente">Seleccione paciente</label>
                    <select class="form-control" id="selectPaciente" name="id_paciente" required>
                        @foreach ($pacientes as $paciente)
                            <option value="{{$paciente->id}}" @if(isset($cita) and $paciente->id == $cita->paciente_info->id) selected @endif >{{ $paciente->firstname}} {{$paciente->surname}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="row mt-2">
                    <label for="selectMedico">Seleccione médico</label>
                    <select class="form-control" id="selectMedico" name="id_medico" required>
                        @foreach ($medicos as $medico)
                            <option value="{{$medico->id}}"  @if(isset($cita) and $medico->id == $cita->paciente_info->id) selected @endif >{{ $medico->firstname}} {{$medico->surname}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="row mt-3">
                    <label for="selectDate">Seleccione la fecha:</label>
                    <input type="date" class="form-control" min="{{date("Y-m-d")}}" name="fecha"  @if( isset($cita) ) value="{{ $cita->fecha }}" @endif required>
                </div>

                <div class="row mt-3">
                    <label for="selectDate">Seleccione la hora:</label>
                    <input type="time" class="form-control" name="hora"  @if( isset($cita) ) value="{{ $cita->hora }}" @endif required>
                </div>
                
                <div class="row mt-3">
                    <div class="mx-auto col-auto">
                        <input type="submit" value="Registrar" class="btn btn-lg btn-outline-primary">
                    </div>
                </div>
            </form>
        </div>
    </section>

@endsection