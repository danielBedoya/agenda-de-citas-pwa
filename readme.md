# Configuraciones y Comandos Importantes

## Instalar el Proyecto

- La versión del PHP que debe usar el apache del servidor deber ser >= 7.0
- **git clone https://gitlab.com/delphianalytics/backoff_senado.git** | Clonar el repositorio
- **chmod 775 -R storage/** | Darle permisos a la carpeta storage
- **composer install** | Instalar modulos y dependencias necesarias
- **cp .env.example .env** | crear el .env y configurarlo
- **php artisan key:generate** | generar clave necesaria para el proyecto

- **mkdir 'public/vendor'** | Crear directorio para los paquetes de diseño (Bootstrap, FontAwesome, JQuery)
- **php artisan vendor:publish --tag=public --force** | copiar package del vendor al public/

## Comandos para Migración de Tablas a la Base de Datos

- **php artisan migrate** | Executa todas las migraciones
- **php artisan migrate:reset** | Elimina todas las tablas
- **php artisan migrate:refresh** | Hace el reset y executa las migraciones
- **php artisan make:migration create_users_table** | Crea una migración para actualizar la base de datos

## Comandos para Correr los Seeders (Datos Semilla)
- **composer dump-autoload** | Actualiza la creación de Factories y Seeders
- **php artisan migrate:refresh --seed** | Correr de nuevo las Migraciones y los Seeders
- **php artisan db:seed** | Correr Seeders