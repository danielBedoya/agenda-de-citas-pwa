<?php

use Illuminate\Database\Seeder;

class firstUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'firstname'=>'Administrador',
            'secondname'=>'admin',
            'surname' => '2020',
            'secondsurname' => '2020',
            'email' => 'admin@localhost.com',
            'numberdoc' => '1234567890', 
            'password' => bcrypt("admin2020"),
            'telefono'=> 3333335,
            'tipo_usuario'=>'administrador'
        ]);
    }
}
