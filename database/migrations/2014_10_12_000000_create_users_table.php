<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('secondname')->nullable();
            $table->string('surname');
            $table->string('secondsurname')->nullable();
            $table->string('email')->unique();
            $table->integer('numberdoc')->unique();
            $table->integer('numbertarjet')->nullable();
            $table->string('especialidad')->nullable();
            $table->string('password');
            $table->string('tipo_usuario');
            $table->integer('telefono')->nullable();
            $table->rememberToken();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
