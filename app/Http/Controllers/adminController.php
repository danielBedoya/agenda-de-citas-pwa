<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InfoCitas;
use app\User;

class adminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $citas = InfoCitas::all();
        $users = User::all();
        return view('admin.index', compact('citas', 'users'));
    }

    public function register($quien)
    {
        return view('admin.register', compact('quien'));
    }

    public function saveuser(Request $request, $quien)
    {
        $password = bcrypt($request->password);
        $data = array_merge($request->all(), array('tipo_usuario' => $quien, 'password' => $password));
        $newUser = User::create($data);
        return view('admin.index', ['updated' => 'true']);
    }

    public function show($quien)
    {
        $users = User::where('tipo_usuario', $quien)->get();
        return view('admin.show', compact('users', 'quien'));
    }

    public function showedit($id)
    {
        $user = User::find($id);
        return view('admin.editar', compact('user'));
    }

    public function edit(Request $request, $id)
    {
        $user = User::find($id);
        $user->firstname = $request->firstname;
        $user->secondname = $request->secondname;
        $user->surname = $request->surname;
        $user->secondsurname = $request->secondsurname;
        $user->email = $request->email;
        $user->numberdoc = $request->numberdoc;
        $user->numbertarjet = $request->numbertarjet;
        if ($request->password != '') {
            $user->password = bcrypt($request->password);
        }
        $user->telefono = $request->telefono;

        $user->save();

        return view('admin.index', ['updated' => 'true']);
    }

    public function delete($id)
    {
        $cita = InfoCitas::where('id_paciente',$id)->orWhere('id_medico',$id);
        if(isset($cita)){
            $cita->id_paciente = null;
            $cita->id_medico = null;
            $cita->delete();
        }
        $user = User::find($id);

        $user->delete();
        return view('admin.index', ['deleted' => 'true']);
    }

    public function registercita()
    {
        $medicos = User::where('tipo_usuario', 'medico')->get();
        $pacientes = User::where('tipo_usuario', 'paciente')->get();
        return view('admin.registercita', compact('medicos', 'pacientes'));
    }

    public function savecita(Request $request)
    {
        $cita = InfoCitas::create($request->all());
        return view('admin.index', ['updated' => 'true']);
    }

    public function showcitas(Request $request)
    {
        $citas = InfoCitas::all();
        return view ( 'admin.showcitas', compact('citas') );
    }

    public function showcita($id)
    {
        $cita = InfoCitas::find($id);
        $medicos = User::where('tipo_usuario', 'medico')->get();
        $pacientes = User::where('tipo_usuario', 'paciente')->get();
        return view ( 'admin.registercita', compact('cita', 'pacientes', 'medicos') );
    }

    public function editcita (Request $request, $id) 
    {
        $cita = InfoCitas::find($id);
        $cita->id_medico = $request->id_medico;
        $cita->id_paciente = $request->id_paciente;
        $cita->fecha = $request->fecha;
        $cita->hora = $request->hora;

        $cita->save();
        return view('admin.index', ['updated' => 'true']);
    }

    public function deletecita ($id)
    {
        $cita = InfoCitas::find($id);
        $cita->delete();
        return view('admin.index', ['deleted' => 'true']);
    }
}
