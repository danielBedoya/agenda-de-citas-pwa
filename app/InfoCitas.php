<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoCitas extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'infocitas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_paciente','id_medico','fecha','hora','estado'];

    /**
     * Obtener la informacion del audio asociada a un paciente.
     */
    public function paciente_info() {
        return $this->hasOne('App\User', 'id', 'id_paciente');
    }

    /**
     * Obtener la informacion del audio asociada a un paciente.
     */
    public function medico_info() {
        return $this->hasOne('App\User', 'id', 'id_medico');
    }



}
